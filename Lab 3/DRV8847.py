''' 
    @file DRV8847.py
    @brief      This file creates motor objects of the DRV8847 motor.
    @details    This creates motor objects, can enable and disable the motors, 
                and sends information out if a fault is triggered.
                
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 1, 2021
 '''
import pyb,utime

class DRV8847:
    ''' 
    @brief      A motor driver class for the DRV8847 from TI.
    @details    Objects of this class can be used to configure the DRV8847
                motor driver and to create one or moreobjects of the
                Motor class which can be used to perform motor
                control.
    
                Refer to the DRV8847 datasheet here:
                https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__(self, pin_sleep, pin_fault, fault_found):
        ''' 
        @brief  Initializes and returns a DRV8847 object.
        @param  pin_sleep   Output pin to enable/disable motor.
                pin_fault   Output pin to trigger a fault from the motors.
                fault_found Contains information if a fault was detected.
        '''
        
        self.pin_sleep = pyb.Pin (pin_sleep, pyb.Pin.OUT_PP)
        self.pin_fault = pyb.ExtInt(pin_fault, mode=pyb.ExtInt.IRQ_FALLING,
                               pull=pyb.Pin.PULL_DOWN, callback=self.fault_cb)
        self.fault_found = fault_found

    def enable(self):
       ''' 
       @brief Brings the DRV8847 out of sleep mode.
       '''
       self.pin_fault.disable()
       self.pin_sleep.high()
      
       utime.sleep_us(25)
       self.pin_fault.enable()
       self.fault_found.write(False)

    def disable(self):
       ''' 
       @brief Puts the DRV8847 in sleep mode.
       '''
       self.pin_sleep.low()

    def fault_cb(self, IRQ_src):
       ''' 
       @brief Callback function to run on fault condition.
       @param IRQ_src The source of the interrupt request.
       '''
       self.pin_sleep.low()
       print("Fault detected! Press 'c' to clear the fault.")
       self.disable()
       self.fault_found.write(True)
       
       

    def motor(self, ch_1, ch_2, pin_1, pin_2,timer):
       ''' 
       @brief Initializes and returns a motor object associated with the DRV8847.
       @return An object of class Motor
       '''

       return Motor(ch_1, ch_2, pin_1, pin_2,timer)

class Motor:
    ''' 
    @brief A motor class for one channel of the DRV8847.
    @details Objects of this class can be used to apply PWM to a given
    DC motor.
    '''
    def __init__(self,ch_1, ch_2, pin_1, pin_2,timer):
        ''' 
        @brief      Initializes and returns a motor object associated with the DRV8847.
        @details    A DRV8847 object is created and then Motor objects are made
                    from that object using the command DRV8847.motor().
        @param  ch_1    A number designating the first channel of the motor.
                ch_2    A number designating the second channel of the motor.
                pin_1   A number designating the first pin of the motor. 
                pin_2   A number designating the second pin of the motor. 
                timer   A number designating the timer pin of the motor.
        '''
        tim = pyb.Timer(timer,freq = 20000)
        self.tim_chan1 = tim.channel(ch_1, mode = pyb.Timer.PWM, pin = pin_1)
        self.tim_chan2 = tim.channel(ch_2, mode = pyb.Timer.PWM, pin = pin_2)
        self.duty = 0
    def set_duty(self, duty):
       ''' 
       @brief   Set the PWM duty cycle for the motor channel.
       @details This method sets the duty cycle to be sent to the motor to the 
       given level. Positive values cause effort in one direction, negative values
       in the opposite direction.
       
       @param duty A signed number holding the duty of the PWM signal 
                   sent to the motor.
       '''
       self.duty =duty
       #Sets duty value to just be 100 if a value too large/small is entered
       
       if self.duty>100:
           self.duty = 100
       elif self.duty<-100:
           self.duty = -100
         
       if self.duty > 0:
           self.tim_chan1.pulse_width_percent(abs(self.duty))
           self.tim_chan2.pulse_width_percent(0)
       elif self.duty<0:
           self.tim_chan1.pulse_width_percent(0)
           self.tim_chan2.pulse_width_percent(abs(self.duty))
       if self.duty==0:
           self.tim_chan1.pulse_width_percent(0)
           self.tim_chan2.pulse_width_percent(0)