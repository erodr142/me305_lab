# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 12:59:05 2021

@author: melab15
"""
import pyb


pinA15 = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pinA15.high()

tim = pyb.Timer(3,freq = 20000)

TIM3_CH1 = tim.channel(1, mode = pyb.Timer.PWM, pin = pyb.Pin.cpu.B4)
TIM3_CH2 = tim.channel(2, mode = pyb.Timer.PWM, pin = pyb.Pin.cpu.B5)

TIM3_CH3 = tim.channel(3, mode = pyb.Timer.PWM, pin = pyb.Pin.cpu.B0)
TIM3_CH4 = tim.channel(4, mode = pyb.Timer.PWM, pin = pyb.Pin.cpu.B1)


#Forwards
TIM3_CH1.pulse_width_percent(100)
TIM3_CH2.pulse_width_percent(0)

TIM3_CH3.pulse_width_percent(100)
TIM3_CH4.pulse_width_percent(0)

#Reverse
TIM3_CH1.pulse_width_percent(0)
TIM3_CH2.pulse_width_percent(100)

TIM3_CH3.pulse_width_percent(0)
TIM3_CH4.pulse_width_percent(100)


#Coast
TIM3_CH1.pulse_width_percent(0)
TIM3_CH2.pulse_width_percent(0)

TIM3_CH3.pulse_width_percent(0)
TIM3_CH4.pulse_width_percent(0)

#Brake
TIM3_CH1.pulse_width_percent(100)
TIM3_CH2.pulse_width_percent(100)

TIM3_CH3.pulse_width_percent(100)
TIM3_CH4.pulse_width_percent(100)