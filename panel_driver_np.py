# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file 
@brief 
@details This file contains 6 function files to read and write to the IMU BNO055.

@author Sangmin Sung
@author Travis Welch
@date November 16, 2021
"""
import pyb, utime
# import numpy as np
  

class Panel_driver:
    ''' 
    @brief 
    @details 
    '''
 
    def __init__(self):
        ''' 
        @brief 
        @details 
        @param 
        '''
        
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        
        self.width = 100 ## y width of board
        self.length = 176 ## x length of board
        
        self.x_c = 0
        self.y_c = 0
        self.z_c = False
        
        self.ts = 0
        self.x_hat = 0
        self.vx_hat = 0
        self.y_hat = 0
        self.vy_hat = 0
        
        # self.matrix_a = np.array([])
        # self.matrix_b = np.array([]) 
        
        self.adc_x = 0
        self.adc_y = 0
        self.adc_z = 0
        
        self.calibration = False

    def x_scan(self):
        ''' 
        @brief  
        @details 
        
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.IN)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.xp.high()
        utime.sleep(0.000007)
        self.xm.low()
        
        adc = pyb.ADC(self.ym)
        self.adc_x = adc.read()
        
        self.xb = self.adc_x*(self.length/4095)-(self.length/2)
        return self.xb
        
    def y_scan(self):
        ''' 
        @brief 
        @details 
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.IN)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        self.yp.high()
        utime.sleep(0.000007)
        self.ym.low()
        
        adc = pyb.ADC(self.xm)
        self.adc_y = adc.read()
        
        self.yb = self.adc_y*(self.width/4095)-(self.width/2)
        return self.yb
        
            
    def z_scan(self):
        ''' 
        @brief 
        @details 
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.yp.high()
        utime.sleep(0.000007)
        self.xm.low()
    
        adc = pyb.ADC(self.xp)
        self.adc_z1 = adc.read()
        if self.adc_z1 >= 5:
            self.z = True
        else:
            self.z = False
        return self.z
    
        
    # def calibration_coeff(self,x,y):
    #     self.matrix_a = np.append([x,y])
    #     self.matrix_b = np.append(self.adc_x,self.adc_y)
    #     if len(self.matrix_a) == 5:
    #         matrix_bTranspose = self.matrix_b.transpose()
    #         self.calibration_variables = np.linalg.inv(matrix_bTranspose @ self.matrix_b) @ matrix_bTranspose @ self.matrix_a
    #         self.matrix_a = np.delete(self.matrix_a,[0,1,2,3,4])
    #         self.matrix_b = np.delete(self.matrix_b,[0,1,2,3,4])
    #         self.calibration = True
        
    # def calibration(self,cali_coef):
    #     matrix_A = np.array(cali_coef[0],cali_coef[1])
    #     matrix_B = np.array(self.adc_x,self.adc_y)
    #     matrix_C = np.array(cali_coef[2][0],cali_coef[2][1])
    #     self.output_xy = matrix_A @ matrix_B + matrix_C
    
    def panel_filter(self,x,y):
        ''' 
        @brief 
        @details  
        '''
        if self.ts == 0:
            self.initial_time = utime.ticks_ms()
            self.x_hat = 0.85*x
            self.y_hat = 0.85*y
        else:
            self.current_time = utime.ticks_ms()
            self.ts = self.current_time -self.initial_time
            self.vx_hat = self.vx_hat + 0.005/self.ts*(x-self.x_hat)
            self.vy_hat = self.vy_hat + 0.005/self.ts*(y-self.y_hat)
            self.x_hat = self.x_hat*0.85*(y-self.y_hat)+self.ts*self.vx_hat
            self.y_hat = self.y_hat*0.85*(y-self.y_hat)+self.ts*self.vy_hat
            self.initial_time = self.current_time
        return self.x_hat,self.y_hat
if __name__ == '__main__':  
    
    panel = Panel_driver()
    # while(panel.calibration == True):
    while(True):
        #     panel.calibration_coeff(panel.x_scan(),panel.y_scan())
        x,y = panel.panel_filter(panel.x_scan(),panel.y_scan())
        print("x position",x,"y position", y)

  