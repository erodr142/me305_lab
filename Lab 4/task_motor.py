'''
    @file       task_motor.py
    @brief      This file is responsible for the logic and decision making of the motor objects.
    @details    The main function of this file is to import motor and encoder 
                data collected from the file DRV8847.py. Takes information to 
                enables the motors to run if a button is pressed from task_user.
                
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 1, 2021
'''

class Task_Motor:
    '''
    @brief      A standard class for assigning tasks to DRV8847. 
    @details    Assigns tasks to DRV8847 objects, including duty and enabling
                information.
    '''
    def __init__ (self, motor_object, motor_driver, enable):
        '''
        @brief  Constructs a Task_Motor object.
        @param  motor_object    An object of the DRV8847 motor.
                motor_driver    A driver for the DRV8847 motors.
        '''
        self.motor_object = motor_object
        self.motor_driver = motor_driver
        self.enable = enable
        
    def run(self,duty):
        '''
        @brief      Runs the Task_Motor class.
        @details    Sets the duty for the motors as entered in task_user. If 
                    the shared enable task is True, re-enables the motors.
        '''
        self.motor_object.set_duty(duty)
        if(self.enable.read()==True):
            self.motor_driver.enable()