'''
   @file                task_controller.py
   @brief               Task controller for the ClosedLoop driver 
   @details             This file is responsible for logic of the ClosedLoop driver. 
                        Responsible for implementing the closed-loop full-state-feedback controller with
                        any additional features required to improve the performance of the balancing platform.

    @author Eddy Rodriguez
    @author Chloe Chou
    @date December 8, 2021
'''


import ClosedLoop, utime

class Task_Controller:
    '''
    @brief      Class of task_controller. 
    @details    This is the class of the task_controller that returns values from the 
                ClosedLoop program and processes this information as a motor duty.
    '''
    def __init__(self,period, ball_position, ball_velocity, IMU_angle, IMU_vels, balance_table, balance_ball):
        '''
        @brief Initializes and returns a Task_Controller object.
        @param period           An integer of the frequency at which the encoder returns data
        @param ball_position    An array of the x and y positions of the ball on the touchscreen
        @param ball_velocity    An array of the x and y velocities of the ball on the touchscreen
        @param IMU_angle        An array of the pitch and roll values of the IMU 
        @param IMU_vels         An array of the x and y angular velocities of the IMU
        @param balance_table    A boolean value if the system is to start balancing the table
        @param balance_ball     A boolean value if the system is to start balancing the ball
        '''
        self.ball_position = ball_position
        self.IMU_angle= IMU_angle
        self.ball_velocity = ball_velocity
        self.IMU_vels = IMU_vels
        self.balance_table = balance_table
        self.balance_ball = balance_ball
        
        self.closedloop = ClosedLoop.ClosedLoop(self.ball_position,
                                                self.IMU_angle,
                                                self.ball_velocity,
                                                self.IMU_vels)
        self.point_value = self.closedloop.run()
        
                
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)

    def run(self):
        '''
        @brief This method calls the ClosedLoop program and controls the rate at which the driver class is ran. 
        @return Returns the new duty that located in the driver run method. If not 
                enough time has passed to calculate a new duty, returns previously
                calculated duty.
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.balance_table.read():
                (x,y) = self.IMU_angle.read()
                duty_x = y*4
                duty_y = -x*4
                
                if abs(y) > 10:
                    duty_x = y*2
                elif abs(y) < 4:
                    duty_x = y*7
                
                if abs(x) > 10:
                    duty_y = -x*2
                elif abs(y) < 4:
                    duty_y = -x*7
                self.point_value = (duty_x, duty_y)
                return (duty_x, duty_y)
            else:
                self.point_value = self.closedloop.run()
                return self.closedloop.run()
        else:
            return self.point_value
        self.next_time = utime.ticks_add(self.next_time, self.period)