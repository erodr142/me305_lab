'''
    @file       task_touchscreen.py
    @brief      This file is responsible for the logic and decision making of the touchscreen.
    @details    This file is responsible for calibration of the the touchscreen 
                and reading off values that can be processed as positions and velocities.
                
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: December 8, 2021
'''
import touchscreen, os

class Task_Touchscreen:
    '''
    @brief      Class of task_touchscreen. 
    @details    This is the class of the task_touchscreen that returns values from the 
                touchscreen and processes this information to return to the system.  
    '''
    def __init__ (self, pin_ym, pin_xm, pin_yp, pin_xp, is_cal, start_cal, ball_position,ball_velocity):
        '''
        @brief Initializes and returns a task_touchscreen object.
        @param period           An integer of the frequency at which the encoder returns data
        @param pin_ym           A Pin object that designates the pin location of pin ym.
        @param pin_xm           A Pin object that designates the pin location of pin xm.
        @param pin_yp           A Pin object that designates the pin location of pin yp.
        @param pin_xp           A Pin object that designates the pin location of pin xp.
        @param is_cal           A boolean value if the touchscreen has been calibrated yet
        @param start_cal        A boolean value if the touchscreen should start calibrating
        @param ball_position    An array of the x and y positions of the ball on the touchscreen
        @param ball_velocity    An array of the x and y velocities of the ball on the touchscreen
        '''
        self.screen = touchscreen.Touchscreen(pin_ym, pin_xm, pin_yp, pin_xp)
        
        self.start_cal = start_cal
        self.is_cal = is_cal
        self.ball_position = ball_position
        self.ball_velocity = ball_velocity
        
        self.gains = [0, 0, 0, 0, 0, 0]
        self.Kxx = 0
        self.Kxy = 0
        self.Kyx = 0
        self.Kyy = 0
        self.Xc = 0
        self.Yc = 0
        
    def run(self):
        '''
        @brief      This function runs the calibration and reading in the task_touchscreen command.
        @details    This function first checks if there is a pre-written file to calibrate the 
                    touchscreen, and if not accepts user input to calibrate the touchscreen. 
                    It then reads off values of the touchscreen, processing the data 
                    as both a position and a velocity.
        '''
        filename = "RT_cal_coeffs.txt"
        if self.start_cal.read() == True:
            if filename in os.listdir():
                print ("File found.")
                # File exists, read from it
                with open(filename, 'r') as f:
                # Read the first line of the file
                    cal_data_string = f.readline()
                    # Split the line into multiple strings and then convert each one to a float
                    self.gains = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                    self.is_cal.write(True)
            else:
                print ("No calibration file found. Starting calibration.")
                # File doesnt exist, calibrate manually and write the coefficients to the file
                with open(filename, 'w') as f:
                    # Perform manual calibration
                    Kxx, Kxy, Kyx, Kyy, Xc, Yc = self.screen.calibrate()
                    self.gains = [Kxx, Kxy, Kyx, Kyy, Xc, Yc]
                    
                    # Then, write the calibration coefficients to the file as a string. 
                    f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                    self.is_cal.write(True)
        x, y = self.screen.scan(self.gains)
        
        if x<200 and y<200:
            fil_x, fil_y,vel_x,vel_y  = self.screen.low_filter(x,y)
            self.ball_position.write([fil_x, fil_y])
            self.ball_velocity.write([vel_x,vel_y])
        else:
            self.ball_position.write([0, 0])
            self.ball_velocity.write([0,0])