'''
    @file       task_user.py
    @brief      This file is responsible for the user interface of the ball balance commands.
    @details    This file changes, first starting from calibrating the touch panel, then the 
                IMU board, and then allows user input to start the table's and ball's balance. 
                While balancing the ball, the user may additionally choose to gather 
                and print data of the balance that can be ended prematurely.
                This can be done with either uppercase or lowercase letters.

    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: December 8, 2021
'''
import utime,pyb

S0_INIT = 0
S1_TOUCH_CAL = 1
S2_IMU_CAL = 2
S3_USER_INPUT = 3
S4_BALANCE_TABLE = 4
S5_BALANCE_BALL = 5
S6_GATHER_DATA = 6
S7_PRINT_DATA = 7

class Task_User:
    '''
    @brief      Class of task_user.
    @details    This is the class of task_user that interfaces with the user and
                supplies data from the user to the rest of the system.   
    '''
    def __init__ (self, period, touch_cal, start_touch_cal, IMU_cal, start_IMU_cal,
                  IMU_angle, IMU_vels, ball_position, ball_velocity, balance_ball, balance_table):

        '''
        @brief Initializes and returns a task_IMU object.
        @param period           An integer of the frequency at which the encoder returns data
        @param touch_cal        A boolean value if the touchscreen has been calibrated yet
        @param start_touch_cal  A boolean value if the touchscreen should start calibrating
        @param IMU_cal          A boolean value if the IMU has been calibrated yet
        @param start_IMU_cal    A boolean value if the touchscreen should start calibrating
        @param IMU_angle        An array of the pitch and roll values of the IMU 
        @param IMU_vels         An array of the x and y angular velocities of the IMU
        @param ball_position    An array of the x and y positions of the ball on the touchscreen
        @param ball_velocity    An array of the x and y velocities of the ball on the touchscreen
        @param balance_ball     A boolean value if the system is to start balancing the ball
        @param balance_table    A boolean value if the system is to start balancing the table
        '''
        self.touch_cal = touch_cal
        self.start_touch_cal = start_touch_cal
        self.IMU_cal = IMU_cal
        self.start_IMU_cal = start_IMU_cal
        
        self.IMU_angle = IMU_angle
        self.IMU_vels = IMU_vels
        self.ball_position = ball_position
        self.ball_velocity = ball_velocity
        self.balance_table = balance_table
        self.balance_ball = balance_ball
        
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)
        
        self.state = S0_INIT
        self.num_st = ""
        self.my_list_pos = []
        self.ser_port = pyb.USB_VCP()
        self.runs = 0
        
    def run(self):
        '''
        @brief      This function runs different states in the task_user command.
        @details    This function oscillates between different states: the initiation
                    state, the calibration state for the touchscreen, the calibration 
                    state for the IMU, waiting for a user command, balancing the table and 
                    ball, and collecting and printing data from the ball's position.
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                print ("____________________________________________________")
                print ("|                                                  |")
                print ("|        Welcome to ME 305 Final Project           |")
                print ("|     created by Chloe Chou and Eddy Rodriguez     |")
                print ("|__________________________________________________|")
                print ("")
                print ("Starting touchscreen calibration. Searching for file.")
                self.state = S1_TOUCH_CAL
                
            elif self.state == S1_TOUCH_CAL:
                if(self.touch_cal.read()==False):
                    self.start_touch_cal.write(True)
                else:
                    print ("Finished touchscreen calibration. Starting IMU calibration.")
                    print ("Please rotate board until 0b11111111 is displayed.")
                    self.state = S2_IMU_CAL
                    self.start_touch_cal.write(False)
                        
            elif self.state == S2_IMU_CAL:
                if(self.IMU_cal.read()==False):
                    self.start_IMU_cal.write(True)

                else:
                    print ("")
                    print ("Finished IMU calibration. Starting balance.")
                    print ("_________________________________________________________________")
                    print ("| List of commands for this device:                              |")
                    print ("| T - Balance the table.                                         |")
                    print ("| B - Balance the ball.                                          |")
                    print ("| C - Stop balancing the ball or table.                          |")
                    print ("| G - Gather and print the position of the ball over 10 seconds. |")
                    print ("| S - Prematurely end data collection of ball.                   |")
                    print ("|________________________________________________________________|")
                    self.state = S3_USER_INPUT
                    self.start_IMU_cal.write(False)
                    
                    
            elif self.state == S3_USER_INPUT:
                if(self.ser_port.any()):
                    char_in = self.ser_port.read(1).decode()
                    if(char_in == 't' or char_in == 'T'):
                        print("Balancing table.")
                        self.balance_table.write(True)
                        self.state = S4_BALANCE_TABLE
                        
            elif self.state == S4_BALANCE_TABLE:
                if(self.ser_port.any()):
                    char_in = self.ser_port.read(1).decode()
                    
                    if(char_in == 'b' or char_in == 'B'):
                        print("Balancing ball.")
                        self.balance_table.write(False)
                        self.balance_ball.write(True)
                        self.state = S5_BALANCE_BALL
                    
                    if(char_in == 'c' or char_in == 'C'): 
                        print("Stopping table balance.")
                        self.balance_table.write(False)
                        self.state = S3_USER_INPUT
                        
            elif self.state == S5_BALANCE_BALL:
                if(self.ser_port.any()):
                    char_in = self.ser_port.read(1).decode()
                    
                    if(char_in == 'g' or char_in == 'G'):
                        print ("Gathering data.")
                        self.state = S6_GATHER_DATA
                    
                    if(char_in == 'c' or char_in == 'C'): 
                        print("Stopping ball balance.")
                        self.balance_ball.write(False)
                        self.state = S3_USER_INPUT
            
            elif self.state == S6_GATHER_DATA:
                self.current_time_collect = utime.ticks_us()
                if (utime.ticks_diff(self.current_time_collect, self.next_time_collect) >= 0):
                    
                    self.my_list_pos.append(self.ball_position.read())
                    self.runs = self.runs + 1
                    if(self.ser_port.any()):
                        char_in = self.ser_port.read(1).decode()
                        if(char_in == 's' or char_in == 'S'): 
                            print('Collection interrupted. Ending data collection.')
                            self.runs = 0
                            self.state = S7_PRINT_DATA
                    if(self.runs >= 50):
                       self.interrupt = True
                       print('End data collection.')
                       self.runs = 0
                       self.state = S7_PRINT_DATA
                    self.next_time_collect = utime.ticks_add(self.next_time_collect, self.period)      
                
            elif self.state == S7_PRINT_DATA:
                for x in range(len(self.my_list_pos)):
                    print(self.my_list_pos[x][0], self.my_list_pos[x][1])
                self.my_list_pos.clear()
                self.state = S5_BALANCE_BALL
            
            else:
                raise ValueError('Invalid State') 
            self.next_time = utime.ticks_add(self.next_time, self.period)