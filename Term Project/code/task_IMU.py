'''
    @file       task_IMU.py
    @brief      This file is responsible for the logic and decision making of the IMU sensor.
    @details    This file is responsible for calibration of the the IMU sensor 
                and reading off values that can be processed as angles and angular
                velocities.
                
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: December 8, 2021
'''

import BN0055, utime, pyb
from pyb import I2C

class Task_IMU:
    '''
    @brief      Class of task_IMU. 
    @details    This is the class of the task_IMU that returns values from the 
                IMU sensor and processes this information to return to the system.
    '''
    def __init__ (self, period, is_cal, start_cal, orientation, velocities):
        '''
        @brief Initializes and returns a task_IMU object.
        @param period           An integer of the frequency at which the encoder returns data
        @param is_cal           A boolean value if the IMU has been calibrated yet
        @param start_cal        A boolean value if the touchscreen should start calibrating
        @param orientation      An array of the pitch and roll values of the IMU 
        @param velocities       An array of the x and y angular velocities of the IMU
        '''
        i2c = I2C(1, I2C.MASTER)
        self.imu = BN0055.BN0055(i2c)
        self.imu.mode(12)
        self.stored_cal_coeff = False
        
        self.start_cal = start_cal
        self.is_cal = is_cal
        
        self.orientation = orientation
        self.velocities = velocities
        
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)
        
        self.ser_port = pyb.USB_VCP()
        self.stored_status = 0
        
        
    def run(self):
        
        '''
        @brief      This function runs the calibration and reading in the task_IMU command.
        @details    This function first reads off values of the IMU to calibrate the 
                    IMU sensor, and then reads off values of the IMU, processing
                    the data as both angles and an angular velocity.
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.start_cal.read() == True:
                cal_status = self.imu.calibration_status()
                
                while(cal_status[0] != 0b11111111 and self.stored_cal_coeff == False):
                    
                    if self.stored_status !=bin(cal_status[0]):
                        self.ser_port.write('\u001b[1K')
                        self.ser_port.write('\u001b[20D')
                        
                        self.ser_port.write(bin(cal_status[0]))
                        self.stored_status = bin(cal_status[0])
                    
                    cal_status = self.imu.calibration_status()
                    
                    if (cal_status[0] == 0b11111111):
                        self.ser_port.write('\u001b[1K')
                        self.ser_port.write('\u001b[20D')
                        
                        self.ser_port.write(bin(cal_status[0]))
                        cal_coeff = self.imu.calibration_coef()
                        self.imu.calibration_coef_write(cal_coeff)
                        self.stored_cal_coeff = True
                        self.is_cal.write(True)
                
                if(cal_status[0] != 0b11111111 and self.stored_cal_coeff == True):
                    self.imu.calibration_coef_write(cal_coeff)
                    self.is_cal.write(True)
                    
                if (cal_status[0] == 0b11111111):
                    self.is_cal.write(True)
                    
            heading,pitch,roll_1 = self.imu.euler()
            pitch = self.imu.convert_hex_to_int(pitch)
            roll =  self.imu.convert_hex_to_int(roll_1)
            self.orientation.write([pitch, roll])
            
            vel_x,vel_y,vel_z = self.imu.ang_vel() # printing in deg/sec
            vel_x = self.imu.convert_hex_to_int(vel_x) 
            vel_y = self.imu.convert_hex_to_int(vel_y)
            vel_z = self.imu.convert_hex_to_int(vel_z)
            self.velocities.write([vel_x, vel_y])

            self.next_time = utime.ticks_add(self.next_time, self.period)