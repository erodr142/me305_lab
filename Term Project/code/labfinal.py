'''
    @file       labfinal.py
    @brief      Main file is designed to give tasks.
    @details    This file does not deal with the hardware directly but 
                assigns task to the task_user.py and task_encoder.py files. 
                Task to individual encoders can be assigned here and parameters
                like period, the rate at which the encoder responds, can
                be controlled here as well.
    @author: Eddy Rodriguez
    @author: Chloe Chou
'''

import shares, task_user, task_touchscreen, task_IMU, task_controller,task_motor,DRV8847, pyb
from pyb import Pin


def main():
    ''' 
    @brief      The main program for the final task
    @details    Tasks for the individual motors, the IMU driver, the touchscreen 
                driver, and the user interface is established here, as well as 
                the data that these tasks collectively share.
    ''' 
    ## @brief   A boolean value if the touchscreen has been calibrated yet
    ## @details This variable is written in task_touchscreen.py and is set to True
    ##          if the touchscreen has been calibrated.
    touch_cal =  shares.Share(False)
    
    ## @brief   A boolean value if the touchscreen should start calibrating
    ## @details This variable is written in task_user.py and is set to True
    ##          when the touchscreen is to begin the calibration process.
    start_touch_cal = shares.Share(False)
    
    ## @brief   A boolean value if the touchscreen should start calibrating
    ## @details This variable is written in task_IMU.py and is set to True
    ##          if the IMU sensor has been calibrated.
    IMU_cal =  shares.Share(False)
    
    ## @brief   A boolean value if the touchscreen should start calibrating
    ## @details This variable is written in task_user.py and is set to True
    ##          when the IMU sensor is to begin the calibration process.   
    start_IMU_cal = shares.Share(False)
    
    ## @brief   An array of the pitch and roll values of the IMU 
    ## @details This array is written in task_IMU.py of values of the pitch and roll
    ##          of the table. It is in the following format: [pitch, roll]. 
    IMU_angle = shares.Share([0, 0])
    
    ## @brief   An array of the x and y angular velocities of the IMU
    ## @details This array is written in task_IMU.py of values of the pitch and roll
    ##          of the table. It is in the following format: 
    ##          [rotation about y-axis, rotation about x-axis]. 
    IMU_vels = shares.Share([0, 0])
    
    ## @brief   A boolean value if the system is to start balancing the ball
    ## @details This variable is written in task_user.py and is set to True
    ##          when the table is to begin balancing the ball.
    balance_ball = shares.Share(False)
    
    ## @brief   A boolean value if the system is to start balancing the table
    ## @details This variable is written in task_user.py and is set to True
    ##          when the table is to begin balancing itself.
    balance_table = shares.Share(False)
        
    ## @brief   An array of the x and y positions of the ball on the touchscreen
    ## @details This variable is written in task_touchscreen.py of values of the 
    ##          x and y positions of the table. It is in the following format: 
    ##          [x-position, y-position]. 
    ball_pos = shares.Share ([0, 0])
    
    ## @brief   An array of the x and y velocities of the ball on the touchscreen
    ## @details This variable is written in task_touchscreen.py of values of the 
    ##          x and y velocities of the table. It is in the following format: 
    ##          [x-velocity, y-velocity]. 
    ball_vels= shares.Share ([0, 0]) 
    
    ## @brief   A variable enable that is true whenever there is not a fault.
    ## @details This variable is written in task_user.py and is set to True
    ##          if the 'c' key is pressed to reset the fault condition.
    enable = shares.Share(True)
    
    ## @brief   A variable fault_found that triggers during a fault.
    ## @details This variable is written in DRV8847.py and is set to True
    ##          if a fault is detected.
    fault_found = shares.Share(False)
    
    motor_drv = DRV8847.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, fault_found)
    motor_drv.enable()  # Enable the motor driver
    
    motor_1 = motor_drv.motor(1,2, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3) #Motor Object 1
    motor_2 = motor_drv.motor(3,4, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3) #Motor object 2

    task1 = task_user.Task_User(100000, touch_cal,start_touch_cal, IMU_cal, start_IMU_cal, 
                                IMU_angle, IMU_vels, ball_pos,ball_vels, balance_ball,balance_table)
    task2 = task_touchscreen.Task_Touchscreen(Pin.cpu.A0, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A7, 
                                              touch_cal,start_touch_cal, ball_pos,ball_vels)
    task3 = task_IMU.Task_IMU(50000, IMU_cal, start_IMU_cal, IMU_angle, IMU_vels)
    task4 = task_controller.Task_Controller(50000, ball_pos, ball_vels, IMU_angle, IMU_vels, balance_table, balance_ball)
    task5 = task_motor.Task_Motor(motor_1,motor_drv,enable, balance_table, balance_ball)
    task6 = task_motor.Task_Motor(motor_2,motor_drv,enable, balance_table, balance_ball)
    
    while(True):
        task1.run()
        task2.run()
        task3.run()
        new_duty_x, new_duty_y = task4.run()
        task5.run(float(new_duty_x))
        task6.run(float(new_duty_y))

if __name__ == '__main__':
    main()