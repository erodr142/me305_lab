# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 12:30:20 2021

@author: melab15
"""

from pyb import Pin, ADC
import utime 

x_scale = (176/4095)
x_offset = 88
y_scale = (100/4095)
y_offset = 50

count = 100
output = Pin.OUT_PP

class touchscreen:
    
    def __init__(self, pin_ym, pin_xm, pin_yp, pin_xp):
        self.pin_ym = pin_ym
        self.pin_xm = pin_xm
        self.pin_yp = pin_yp
        self.pin_xp = pin_xp
        
        #variables for low pass filter
        self.Ts = 0
        self.xhat = 0
        self.vxhat = 0
        self.yhat = 0
        self.vyhat = 0
        
        #For calibration 
        # self.matrix_a = np.array([])
        # self.matrix_b = np.array([]) 
    def scan (self):
        pin_yp = Pin(self.pin_yp, output)
        pin_yp.high()
        pin_xm = Pin(self.pin_xm, output)
        pin_xm.low()
        
        if(float (ADC(self.pin_ym).read())<=4075):
            pin_xp = Pin(self.pin_xp, output)
            pin_xp.high()
            pin_xm = Pin(self.pin_xm, output)
            pin_xm.low()
           
            pin_yp = Pin(self.pin_yp, output)
            pin_yp.high()
            pin_ym = Pin(self.pin_ym, output)
            pin_ym.low()
            
            
            return [float (ADC(self.pin_ym).read())*(x_scale) - x_offset, float (ADC(self.pin_xm).read())*(y_scale) - y_offset]

        else:
            return [0, 0]
    def low_filter(self,x,y):
        if self.Ts ==0:
            self.initial_time  = utime.ticks_us()
            self.xhat = .85*x
            self.yhat = .85*y
        else: 
            self.current_time = utime.ticks_us()
            self.Ts = self.current_time -self.initial_time
            
            self.vxhat = self.vxhat + 0.005/self.Ts*(x-self.xhat)
            self.vyhat = self.vyhat + 0.005/self.Ts*(y-self.yhat)
            
            self.xhat = self.xhat*0.85*(y-self.yhat)+self.Ts*self.vxhat
            self.yhat = self.yhat*0.85*(y-self.yhat)+self.Ts*self.vyhat
            
            self.initial_time = self.current_time
        return self.xhat,self.yhat
    # def calibration_coeff(self,x,y):
    #     self.matrix_a = np.append([x,y])
    #     self.matrix_b = np.append(self.adc_x,self.adc_y)
    #     if len(self.matrix_a) == 5:
    #         matrix_bTranspose = self.matrix_b.transpose()
    #         self.calibration_variables = np.linalg.inv(matrix_bTranspose @ self.matrix_b) @ matrix_bTranspose @ self.matrix_a
    #         self.matrix_a = np.delete(self.matrix_a,[0,1,2,3,4])
    #         self.matrix_b = np.delete(self.matrix_b,[0,1,2,3,4])
    #         self.calibration = True
        
    # def calibration(self,cali_coef):
    #     matrix_A = np.array(cali_coef[0],cali_coef[1])
    #     matrix_B = np.array(self.adc_x,self.adc_y)
    #     matrix_C = np.array(cali_coef[2][0],cali_coef[2][1])
    #     self.output_xy = matrix_A @ matrix_B + matrix_C
if __name__ == '__main__':
    screen = touchscreen(Pin.cpu.A0, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A7)
    
    total = 0
    while(True):
        for i in range(count):
            last_time = utime.ticks_us()
            
            x, y = screen.scan()
            x, y  = screen.low_filter(x,y)
            print('x',x,'y',y)
            current_time = utime.ticks_us()
            diff = utime.ticks_diff(current_time, last_time)
            total += diff
        #print("average:", total/(count))
        total = 0
        diff = 0
    utime.sleep(500000)
        