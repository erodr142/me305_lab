'''
    @file       task_user.py
    @brief      
    @details    


    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: December 8, 2021
'''
import utime,pyb

S0_INIT = 0
S1_TOUCH_CAL = 1
S2_IMU_CAL = 2
S3_BALANCE = 3

class Task_User:
    '''
    @brief
    @details
    '''
    def __init__ (self, period, touch_cal, start_touch_cal, IMU_cal, start_IMU_cal,
                  orientation, velocities, ball_position):

        '''
        @brief 
        @param 
        '''
        self.touch_cal = touch_cal
        self.start_touch_cal = start_touch_cal
        self.IMU_cal = IMU_cal
        self.start_IMU_cal = start_IMU_cal
        
        self.orientation = orientation
        self.velocities = velocities
        self.ball_position = ball_position
        
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)
        
        self.state = S0_INIT
        
    def run(self):
        '''
        @brief      This function runs different states in the task_user command.
        @details    
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                print("____________________________________________________")
                print("|                                                  |")
                print("|        Welcome to ME 305 Final Project           |")
                print("|     created by Chloe Chou and Eddy Rodriguez     |")
                print("|__________________________________________________|")
                print("")
                print ("Starting touchscreen calibration. Searching for file.")
                self.state = S1_TOUCH_CAL
                
            elif self.state == S1_TOUCH_CAL:
                if(self.touch_cal.read()==False):
                    self.start_touch_cal.write(True)
                else:
                    print ("Finished touchscreen calibration. Starting IMU calibration.")
                    print ("Please rotate board until 0b11111111 is displayed.")
                    self.state = S2_IMU_CAL
                    self.start_touch_cal.write(False)
                        
                        
            elif self.state == S2_IMU_CAL:
                if(self.IMU_cal.read()==False):
                    self.start_IMU_cal.write(True)

                else:
                    print ("Finished IMU calibration. Starting balance.")
                    self.state = S3_BALANCE
                    self.start_IMU_cal.write(False)
                    
                    
            elif self.state == S3_BALANCE:
                utime.sleep_us(50)
#                print (self.ball_position.read())
                
                
#                
#                
#                if(self.ser_port.any()):
#                        char_in = self.ser_port.read(1).decode()
#                        
#                        if(char_in.isdigit()): # Adds to string only if the number is a digit
#                            self.kp_str = self.kp_str + char_in
#                            self.ser_port.write(char_in)
#                        
#                        elif (char_in == '-'): # Only adds to string if the minus sign is the first character entered
#                            if (len(self.kp_str)==0):
#                                self.kp_str = self.kp_str + char_in
#                                self.ser_port.write(char_in)
#                        
#                        elif (char_in == '\x7F'): # Backspaces last key and changes num_st appropriately
#                            if (len(self.kp_str)!=0):
#                                self.kp_str = self.kp_str[:-1]
#                                self.ser_port.write(char_in)
#                        
#                        elif (char_in == '.'): # If the string does not already have a decimal, add one
#                            if self.kp_str.find(char_in) == -1:
#                                self.kp_str = self.kp_str + char_in
#                                self.ser_port.write(char_in)
#                        elif (char_in == '\r'or char_in == '\n'): # Submit entered duty
#                            if (len(self.kp_str)==0):
#                                self.kp_str = "0.0"
#                            print ("")
#                            kp_num = float (self.kp_str)
#                            
#                            if self.motor_number == 1:
#                                print('Motor 1 given k of {:}.'.format(kp_num))
#                                self.kp_1.write(kp_num)
#                                
#                                print('Please enter velocity for Motor 1.')
#                                self.state = S3_ENTER_DATA
#                                self.kp_str = "" 
#                                kp_num = 0
#                                
#                            elif self.motor_number == 2:
#                                print('Motor 2 given k of {:}.'.format(kp_num))
#                                self.kp_2.write(kp_num)
#                                print('Please enter velocity for Motor 2.')
#                                self.state = S3_ENTER_DATA
#                                self.kp_str = ""
#                                kp_num = 0
#        
#            elif self.state == S3_ENTER_DATA:
#                if(self.fault_found.read()):
#                        self.state = S6_FAULT
#                
#                if(self.ser_port.any()):
#                        char_in = self.ser_port.read(1).decode()
#                        
#                        if(char_in.isdigit()): # Adds to string only if the number is a digit
#                            self.num_st = self.num_st + char_in
#                            self.ser_port.write(char_in)
#                        
#                        elif (char_in == '-'): # Only adds to string if the minus sign is the first character entered
#                            if (len(self.num_st)==0):
#                                self.num_st = self.num_st + char_in
#                                self.ser_port.write(char_in)
#                        
#                        elif (char_in == '\x7F'): # Backspaces last key and changes num_st appropriately
#                            if (len(self.num_st)!=0):
#                                self.num_st = self.num_st[:-1]
#                                self.ser_port.write(char_in)
#                        
#                        elif (char_in == '.'): # If the string does not already have a decimal, add one
#                            if self.num_st.find(char_in) == -1:
#                                self.num_st = self.num_st + char_in
#                                self.ser_port.write(char_in)
#                        elif (char_in == '\r'or char_in == '\n'): # Submit entered duty
#                            if (len(self.num_st)==0):
#                                self.num_st = "0.0"
#                            print ("")
#                            num_doub = float (self.num_st)
#                            
#                            if self.motor_number == 1:
#                                print('Motor 1 will run at {:} radians/second'.format(num_doub))
#                                print('Running closed loop. Press s to interrupt early.')
#                                self.omega_ref_1.write(num_doub)
#                                self.state = S4_STEP_MOTOR
#                                num_doub = 0
#                                self.num_st = ""
#                                
#                            elif self.motor_number == 2:
#                                print('Motor 2 will run at {:} radians/second'.format(num_doub))
#                                print('Running closed loop. Press s to interrupt early.')
#
#                                self.omega_ref_2.write(num_doub)
#                                self.state = S4_STEP_MOTOR
#                                num_doub = 0
#                                self.num_st = "" 
#                
#            elif self.state == S4_STEP_MOTOR:
#                self.state = S5_PRINT_DATA
#                    
#
#            elif self.state == S5_PRINT_DATA:
#                self.state = S1_WAIT_FOR_CHAR
            
            else:
                raise ValueError('Invalid State') 
            self.next_time = utime.ticks_add(self.next_time, self.period)
       