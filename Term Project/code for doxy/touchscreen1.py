''' 
    @file touchscreen1.py
    @brief      This file creates motor objects of the 4 pin resistive touchsreen.
    @details    The  4 pin resistive touchsreen sensor uses two resistive materials
                that can be used to calculate the position of the ball at all points
                as well as determine if there is a ball on the touchscreen. This 
                device can also be calibrated. 
                
    @author Eddy Rodriguez
    @author Chloe Chou
    @date December 8, 2021
'''
from pyb import Pin, ADC
import utime
import micropython
from micropython import const
from ulab import numpy as np

_X_SCALE = micropython.const(176/4095)
_X_OFF = const(88)
_Y_SCALE = micropython.const(100/4095)
_Y_OFF = const(50)

_OUT = micropython.const(Pin.OUT_PP)
_IN = micropython.const(Pin.IN)

class Touchscreen:
    '''
    @brief      Class of the touchscreen. 
    @details    This is the class of the the touchscreen that returns takes 
    the raw data values and returns position of the ball as well as calibration.
    '''
    
    def __init__(self, pin_ym, pin_xm, pin_yp, pin_xp):
        '''
        @brief Initializes and returns a touchscreen object.
        @param pin_ym           A Pin object that designates the pin location of pin ym.
        @param pin_xm           A Pin object that designates the pin location of pin xm.
        @param pin_yp           A Pin object that designates the pin location of pin yp.
        @param pin_xp           A Pin object that designates the pin location of pin xp.
        '''

        self.ym = pin_ym
        self.xm = pin_xm
        self.yp = pin_yp
        self.xp = pin_xp
        
        # defining variables for low pass filter
        self.Ts = 0
        self.xhat = 0
        self.vxhat = 0
        self.yhat = 0
        self.vyhat = 0
        self.initial_time = 0
        
        self.desired_cal = np.array([[-88, -50], [-88, 50], [88, 50], [80, -50]])
       
    def scan (self, gains):
        '''
        @brief Performs scan to determine ball position
        @param gains    An array of 6 values representing the corrected gains
                        obtained during calibration.
        @return Returns an array of x and y values of the touchscreen
        '''
        yp = Pin(self.yp, _OUT)
        yp.high()
        xm = Pin(self.xm, _OUT)
        xm.low()
        
        # gains: Kxx, Kxy, Kyx, Kyy, Xc, Yc
        if(float (ADC(self.ym).read())<=4075):
            yp = Pin(self.yp, _IN)
            xm = Pin(self.xm, _IN)
            xp = Pin(self.xp, _OUT)
            xp.high()
            xm = Pin(self.xm, _OUT)
            xm.low()
            x_pos = float (ADC(self.ym).read())
            
            xp = Pin(self.xp, _IN)
            xm = Pin(self.xm, _IN)
            yp = Pin(self.yp, _OUT)
            yp.high()
            ym = Pin(self.ym, _OUT)
            ym.low()
            y_pos = float (ADC(self.xm).read())
            
            x_loc = gains[0]*x_pos + gains[1]*y_pos + gains[4]
            y_loc = gains[2]*x_pos + gains[3]*y_pos + gains[5]
            return [x_loc, y_loc]
        else:
            return [200, 200]
        
    def scan_raw (self):
        '''
        @brief Performs "raw" scan to determine ADC values when calibrating
        @return Returns the ADC readings of the board's touchscreen without
                caluclating position 
        '''
        yp = Pin(self.yp, _OUT)
        yp.high()
        xm = Pin(self.xm, _OUT)
        xm.low()
        
        
        if(float (ADC(self.ym).read())<=4075):
            yp = Pin(self.yp, _IN)
            xm = Pin(self.xm, _IN)
            xp = Pin(self.xp, _OUT)
            xp.high()
            xm = Pin(self.xm, _OUT)
            xm.low()
            x_loc = float (ADC(self.ym).read())
            
            xp = Pin(self.xp, _IN)
            xm = Pin(self.xm, _IN)
            yp = Pin(self.yp, _OUT)
            yp.high()
            ym = Pin(self.ym, _OUT)
            ym.low()
            y_loc = float (ADC(self.xm).read())
            
            return [x_loc, y_loc]
        else:
            return [4096, 4096]
        
    def low_filter(self,x,y):
        '''
        @ brief Filters results of a scan's reading
        @ param x The x value of the scan function's results
        @ param y the y value of the scan function's results
        @ return Returns an array of 4 values: the ball's x and y coordinates 
        as well as the ball's x and y velocities after filtering data

        '''
        self.current_time = utime.ticks_us()
        self.Ts = (self.current_time -self.initial_time)/1000000 #Ts is in seconds
        
        self.xhat = self.xhat + 0.85*(x-self.xhat) + self.Ts*self.vxhat #x position in m 
        self.yhat = self.yhat + 0.85*(y-self.yhat) + self.Ts*self.vyhat #y position in m
        
        self.vxhat = self.vxhat + (0.045/self.Ts)*(x-self.xhat) #x velocity in mm/s
        self.vyhat = self.vyhat + (0.045/self.Ts)*(y-self.yhat) #y velocity in mm/s We'll have to change the constants
        
        self.initial_time = self.current_time
        

        return self.xhat, self.yhat , self.vxhat, self.vyhat
    
    def calibrate(self):
        '''
        @ brief Performs initial calibration of the touchscreen
        @ return    Returns an array of 6 values of the gains to be used during
                    the scan function
        '''
        print ("Press down on bottom left corner.")
        [x_bl, y_bl] = self.scan_raw()
        while [x_bl,y_bl] == [4096, 4096]:
            [x_bl, y_bl] = self.scan_raw()
        utime.sleep_us(300000)
        
        print ("Press down on top left corner.")
        [x_tl, y_tl] = self.scan_raw()
        while [x_tl,y_tl] == [4096, 4096]:
            [x_tl, y_tl] = self.scan_raw()

        utime.sleep_us(300000)
        
        print ("Press down on top right corner.")
        [x_tr, y_tr] = self.scan_raw()
        while [x_tr, y_tr] == [4096, 4096]:
            [x_tr, y_tr] = self.scan_raw()
        utime.sleep_us(300000)
        
        print ("Press down on bottom right corner.")
        [x_br, y_br] = self.scan_raw()
        while [x_br,y_br] == [4096, 4096]:
            [x_br, y_br] = self.scan_raw()
        
        cal_ADC = np.array([[x_bl, y_bl, 1], [x_tl, y_tl, 1], [x_tr, y_tr, 1], [x_br, y_br, 1]])
        inverse = np.linalg.inv(np.dot(cal_ADC.transpose(), cal_ADC))
        transed = np.dot(inverse, cal_ADC.transpose())
        
        cal_array = np.dot(transed, self.desired_cal)
        cal_cons = cal_array.flatten()

        print("Calibrated.")
        return cal_cons
                 
#if __name__ == '__main__':
#    screen = Touchscreen(Pin.cpu.A0, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A7)
#    gains = []
#    filename = "RT_cal_coeffs.txt"
#    if filename in os.listdir():
#        # File exists, read from it
#        with open(filename, 'r') as f:
#            # Read the first line of the file
#            cal_data_string = f.readline()
#            # Split the line into multiple strings and then convert each one to a float
#            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
#    else:
#        # File doesnt exist, calibrate manually and write the coefficients to the file
#        with open(filename, 'w') as f:
#            # Perform manual calibration
#            Kxx, Kxy, Kyx, Kyy, Xc, Yc = screen.calibrate()
#            gains = [Kxx, Kxy, Kyx, Kyy, Xc, Yc]
#            # Then, write the calibration coefficients to the file as a string. 
#            # The example uses an f-string, but you can use string.format() 
#            # if you prefer
#            f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
#    
#    x, y = screen.scan(gains)
#    
#    
#    
#    
#    if [x,y] !=[4096, 4096]:
#        new_x, new_y  = screen.low_filter(x,y)
#        print('x',new_x,'y',new_y)