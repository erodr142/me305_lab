'''
    @file       task_motor1.py
    @brief      This file is responsible for the logic and decision making of the motor objects.
    @details    The main function of this file is to import motor and encoder 
                data collected from the file DRV8847.py. Takes information to 
                enables the motors to run if a button is pressed from task_user.
                
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: December 8, 2021
'''

class Task_Motor:
    '''
    @brief      A standard class for assigning tasks to DRV8847. 
    @details    Assigns tasks to DRV8847 objects, including duty and enabling
                information.
    '''
    def __init__ (self, motor_object, motor_driver, enable, balance_table, balance_ball):
        '''
        @brief  Constructs a Task_Motor object.
        @param  motor_object    An object of the DRV8847 motor.
        @param  motor_driver    A driver for the DRV8847 motors.
        '''
        self.motor_object = motor_object
        self.motor_driver = motor_driver
        self.enable = enable
        self.balance_table = balance_table
        self.balance_ball = balance_ball
        
    def run(self,duty):
        '''
        @brief      Runs the Task_Motor class.
        @details    Sets the duty for the motors as entered in task_user. If 
                    the shared enable task is True, re-enables the motors.
        '''
        if self.balance_table.read() or self.balance_ball.read():
            self.motor_object.set_duty(duty)
            if(self.enable.read()==True):
                self.motor_driver.enable()
        else:
            self.motor_object.set_duty(0)