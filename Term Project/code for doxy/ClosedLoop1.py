'''
    @file       ClosedLoop1.py
    @brief      ClosedLoop feedback control for the DC motors
    @details    A PID controller is used to reach desired motor velocity. The 
                user inputs a Kp, proportional gain, and a desired motor velocity 
                which are both shared to this closedloop driver. This driver 
                calculates a new duty cycle that is then shared back to the task_controller. 

    @author Eddy Rodriguez
    @author Chloe Chou
    @date December 8, 2021
'''
from ulab import numpy as np

class ClosedLoop:
    '''
    @brief      Class of ClosedLoop. 
    @details    This is the class of the ClosedLoop that returns a duty of the
                motors when taking in the position and velocity of the ball and 
                orientation and angular velocity of the table.
    '''
    def __init__(self,ball_position,IMU_angle,ball_velocity,IMU_vels):
        '''
        @brief Initializes and returns a ClosedLoop object.
        @param IMU_angle        An array of the pitch and roll values of the IMU 
        @param IMU_vels         An array of the x and y angular velocities of the IMU
        @param ball_position    An array of the x and y positions of the ball on the touchscreen
        @param ball_velocity    An array of the x and y velocities of the ball on the touchscreen
        '''
        
        self.A = np.array([[0,0,1,0],[0,0,0,1],[-5.2170 ,4.011 ,0,0],[112.8871, 64.8295, 0,0]])
        self.B = np.array([[0],[0],[32.4991],[-703.2272]])
        
        #self.K1 = np.array([[.0000035,.0015,.00000055, .0000025]]) # System gains for full state feedback for motor 1 x-axis
        #self.K2 = np.array([[.0000035,.0015,.00000055, .0000025]]) # System gains for full state feedback for motor 2 y-axis
        self.Vdc = 12 
        self.R = 2.21 #armature resistance ohm 
        self.Kt = 13.8*.001 #motor constant Nm/A
        
        
        self.ball_position = ball_position #ball postion as a 1x2 array object
        self.ball_position_x = 0 #ball postion-x from touchscreen
        self.ball_position_y = 0 #ball postion-y from touchscreen
        
        self.IMU_angle= IMU_angle #panel position angle from IMU as a 1x2 array object
        self.IMU_angle_x = 0 #panel position x angle from IMU
        self.IMU_angle_y = 0#panel position x angle from IMU
        
        self.ball_velocity = ball_velocity #ball velocity from touchscreen as a 1x2 array object
        self.ball_vel_x = 0 #ball velocity-x from touchscreen
        self.ball_vel_y = 0 #ball velocity-y from touchscreen
        
        self.IMU_vels = IMU_vels  #panel omega deg/s from IMU as a 1x2 array object
        self.IMU_vel_x = 0 #panel omega x deg/s from IMU
        self.IMU_vel_y = 0 #panel omega y deg/s from IMU
        
    def run(self):
        '''
        @brief Runs controller function 
        @details The function runs the closedloop feedback system. New calculated duty is below -100 or above 100
                 then the duty is set to the maximum duty of either -100 or 100. 
        @return This function returns the new calculated duty cycle for the DC motor 
        '''
        #print('State vector', self.SVx)
        
        self.ball_position_x, self.ball_position_y = self.ball_position.read()
        self.IMU_angle_x, self.IMU_angle_y = self.IMU_angle.read()
        self.ball_vel_x, self.ball_vel_y = self.ball_velocity.read()
        self.IMU_vel_x, self.IMU_vel_y = self.IMU_vels.read()
    
        
        #Motor 1 rotates about y axis
        self.k11 = -4.96683*.001
        self.k21 = -7.400*.001 # control imu position rotating about y
        self.k31 = -1.7126*.0008
        self.k41 = -0.50261*.0008 # control imu position rotating about y
        
        #Motor 2 Closest to me  rotates about y axis
        self.k12 = -4.96683*.001 # ball position about y axis 
        self.k22 = -7.40*.001 # control imu position rotating about y
        self.k32 = -1.7126*.0008
        self.k42 = -0.50261*.0008 # control imu omega rotating about y
        # Motor Parameters
        self.Resistance = 2.21 #ohms
        self.K_t = 0.0138 #N-m/A
        self.V_dc = 12 #volts
        
        
        #Motor 1 rotates x axis
        T_x = (self.ball_position_x * self.k11 + 
               self.IMU_angle_y * self.k21 + 
               self.ball_vel_x * self.k31 + 
               self.IMU_vel_y * self.k41)
        #Motor 1 rotates y axis 
        T_y = (self.ball_position_y * self.k12 + 
               self.IMU_angle_x * self.k22 + 
               self.ball_vel_y * self.k32 + 
               self.IMU_vel_x * self.k42)
        
        self.duty_x = (100 * self.Resistance * T_x) / (4 * self.K_t * self.V_dc)
        self.duty_y = (100 * self.Resistance * T_y) / (4 * self.K_t * self.V_dc)
#        print('duty x,',self.duty_x,'duty y,',self.duty_y,)
        
        return -self.duty_x, self.duty_y
        # self.SVx = np.array([[self.ball_position_x],[self.IMU_angle_x],[self.ball_vel_x],[self.IMU_vel_x]])
        # self.SVy = np.array([[self.ball_position_y],[self.IMU_angle_y],[self.ball_vel_y],[self.IMU_vel_y]])
        
        
        # self.ACx = self.A- np.dot(self.B,self.K1)
        # self.SVx = np.dot(self.ACx, self.SVx)
        # self.Tx = -np.dot(self.K1,self.SVx)#Motor torque for x
        # self.Cx = (100*self.R/(4*self.Kt*self.Vdc))
        # self.Dx = self.Cx*self.Tx
        
        # self.ACy = self.A - np.dot(self.B,self.K2)
        # self.SVy = np.dot(self.ACy, self.SVy)
        # self.Ty = -np.dot(self.K2,self.SVy)#Motor torque for x
        # self.Cy = (100*self.R/(4*self.Kt*self.Vdc))
        # self.Dy = self.Cy*self.Ty 
        
        # self.duty_x = self.Dx[0][0]
        # self.duty_y = self.Dy[0][0]
        
        # if self.duty_x >100:
        #     self.duty_x = 100
        # if self.duty_x <-100:
        #     self.duty_x = -100
            
        # if self.duty_y >100:
        #     self.duty_y = 100
        # if self.duty_y <-100:
        #     self.duty_y = -100 
        #print('Converted duty', self.duty_x , self.duty_y)
        #return self.duty_x , self.duty_y