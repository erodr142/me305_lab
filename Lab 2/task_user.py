'''
@file       task_user.py
@brief      This file is responsible for the logic and decision making of the encoder.
@details    Depending on the input of the user the task_user.py will return either position, 
            delta, or time. If the user inputs an invalid argument they will be notified 
            and will wait for a valid argument. 
@image html SStask_user.jpg "State Space Diagram:Task Diagram" width=600px
@image html lab2task.png "Task Diagram" width=600px
'''
"""
Created on Tue Oct 12 11:47:45 2021

@author: Eddy Rodriguez
@author: Chloe Chou
"""
import utime,pyb

S0_INIT = 0
S1_WAIT_FOR_CHAR = 1
S2_COLLECT_DATA = 2
S3_PRINT_DATA = 3
S4_DUTY1 = 4
S4_DUTY2 = 5
class Task_User:
    '''
    @brief
    @details
    '''
    def __init__ (self,period, enc_pos, delta_pos, enc_zero):
        '''
        @brief Constructs task_user objects
        @param period controlls the frequency at which the encoder returns data 
               enc_pos  Returns the encoder position
               delta_pos returns the delta of the encoder over a period 
               enc_zero  Zeros the encoder postion using a datum value 

        Parameters
        ----------
        period : TYPE
            DESCRIPTION.
        enc_pos : TYPE
            DESCRIPTION.
        delta_pos : TYPE
            DESCRIPTION.
        enc_zero : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        self.enc_pos = enc_pos
        self.delta_pos = delta_pos
        self.enc_zero = enc_zero
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)
        self.state = S0_INIT
        self.interrupt = False
        
        self.my_list_runs = []
        self.my_list_position= []
        
        self.runs = 0
        

        self.ser_port = pyb.USB_VCP()
        
        
    def run(self):
        '''
        @brief  runs different states in the task users
        @details  This function stays in state 1 which waits for input of 
                  of the user. It retuns task requested values.
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                print("Welcome, here are a list of commands for this device \n z-Zero the position of encoder 1 \n p-Print out the position of encoder 1 \n d-Print out the delta for encoder 1 \n g-Collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list \n s-End data collection prematurely")
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
                
                if(self.enc_zero.read() == True):
                    self.enc_zero.write(False)
                
                if(self.ser_port.any()):
                    char_in = self.ser_port.read(1)
                    char_decoded = char_in.decode()
                    if(char_decoded == 'z' or char_decoded == 'Z'):
                        print ("Zeroing position. Motor is now at 0.")
                        self.enc_zero.write(True)
                        
                    elif(char_decoded == 'p' or char_decoded == 'P'):
                        print('Motor position is {:}.'.format(self.enc_pos.read()))
                         
                    elif(char_decoded == 'd' or char_decoded == 'D'):
                        print ('Motor delta is {:}.'.format(self.delta_pos.read()))
                    
                    elif(char_decoded == 'g' or char_decoded == 'G'):
                        self.state = S2_COLLECT_DATA
                        self.interrupt = False
                        print ("Collecting Data.")
                        
                    elif(char_decoded == 'm'):
                        self.state = S4_DUTY1
                        duty = input("Please enter duty cycle for motor 1:")
                        
                    elif(char_decoded == 'M'):
                        self.state = S4_DUTY2
                        duty = input("Please enter duty cycle for motor 2:")
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_decoded))
                    
            elif self.state == S2_COLLECT_DATA:
                
                self.current_time_collect = utime.ticks_us()
                
                if (utime.ticks_diff(self.current_time_collect, self.next_time_collect) >= 0):
                    
                    self.my_list_position.append(self.enc_pos.read())
                    self.my_list_runs.append(self.runs)
                    print("Position",self.enc_pos.read())
                    self.runs = self.runs + 1
                    print("runs", self.runs)
                    
                    if(self.ser_port.any()): #breaks while loop if for new s input
                        char_in = self.ser_port.read(1)
                        char_decoded = char_in.decode()
                        
                        if(char_decoded == 's' or char_decoded == 'S'):
                            self.interrupt = True
                            print('End data collection.')
                            self.runs = 0            
                            self.state = S3_PRINT_DATA
                    if(self.runs >= 31):
                        self.interrupt = True
                        print('End data collection.')
                        self.runs = 0
                        self.state = S3_PRINT_DATA
                    self.next_time_collect = utime.ticks_add(self.next_time_collect, self.period)
                    
            elif self.state == S3_PRINT_DATA:
                
                print('Printing data.')
                for x in range(len(self.my_list_position)):
                    print('At Second {:} Position is {:}'.format(self.my_list_runs[x], self.my_list_position[x]))
                self.state = S1_WAIT_FOR_CHAR
            # elif self.state == S4_DUTY1:
            #     #ADD CODE HERE
            
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
