'''
    @file       lab2main.py
    @brief      Main file is designed to give tasks.
    @details    This file does not deal with the hardware directly but 
                assigns task to the task_user.py and task_encoder.py files. 
                Task to individual encoders can be assigned here and parameters
                like period, the rate at which the encoder responds, can
                be controlled here as well. 
                
                
    @image html task diagram.png "Task diagram lab 2" width=600px
                
'''
"""
Created on Tue Oct  5 14:52:49 2021

@author: Eddy Rodriguez
@author: Chloe Chou
"""

import task_encoder,task_user,shares


def main():
    ''' @brief The main program
        @details Tasks for the individual encoders are defined here. 
    ''' 
    
    enc_pos = shares.Share(0)
    delta_pos = shares.Share(0)
    enc_zero = shares.Share(False)
    #enc_time = shares.Share(0)

    
    task1 = task_user.Task_User(1000000, enc_pos, delta_pos, enc_zero)
    task2 = task_encoder.Task_Encoder(50000,4,enc_pos, delta_pos,enc_zero)
    
    ## A list of tasks to run
    task_list = [task1, task2]
    
    
    while(True):
        try:
            for task in task_list:
                task.run()
            
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')

if __name__ == '__main__':
    main()

