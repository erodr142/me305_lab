"""
Created on Tue Sep 28 13:34:29 2021

@file lab0x01.py
@brief Displays LED pattern toggled by a button push to switch between states.
@details Takes user input from a button. Waits for user input before beginning
    the first LED display. All subsequent button pushes switch the LED between
    three states: square wave, sine wave, and sawtooth wave, in that order. 
    When the button is pressed again, the LED returns to flashing in a square
    wave pattern. A video can be found here: https://youtu.be/c_3EfI9Qw_A
@author Chloe Chou
@author Eddy Rodriguez
@date Tue Oct 5 2021
@copyright 2001-2021 Python Software Foundation. All Rights Reserved.
@copyright 2000 BeOpen.com. All Rights Reserved.
@copyright 1995-2001 Corporation for National Research Initiatives. 
    All Rights Reserved.
@copyright 1991-1995 Stichting Mathematisch Centrum, Amsterdam. All Rights Reserved.
\image html Lab1Diagram2.png "State Machine Diagram"
link of the State Machine diagram:https://cpslo-my.sharepoint.com/:i:/g/personal/erodr142_calpoly_edu/Ec2J0B9DYEFAuEwpvzsJcbUBW0GfiIOXgPuyo9lcyhaS3w?e=xntGUU

A video of the code running can be seen here:

\htmlonly
<iframe width="560" height="315" src="https://youtu.be/c_3EfI9Qw_A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly
"""
# All import statements should go near the top
import utime
import time
import pyb
import math

pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)


"""
@brief Rests the value of a timing device.
@details Uses the utime function to determine the starting time of a run and 
    stores that number in the global variable startTime.
"""
def resetTimer():
    global startTime
    startTime = utime.ticks_ms()

"""
@brief Updates the value of a timing device for how much time a process has taken.
@details Uses the utime function to determine the time a process takes. 
    Determines the time at present and subtracts it from the global 
    variable startTime to find the difference. This is assigned to the global 
    variable currentTime.
"""
def updateTimer():
    global currentTime
    stopTime = utime.ticks_ms()
    currentTime = utime.ticks_diff(stopTime, startTime)
    
"""
@brief Returns signals for an LED to perform a sqare wave
@details Given an input time, returns a signal that can 
    be interpreted by an LED as a square wave using a 
    mathematical comparison and the current time of a process.
@param currentTime The current time a process has taken 
    since the timer was last reset.
@return A boolean based on if the mathematical comparison is met.
"""
def update_SQW(currentTime):
    return 100*((currentTime/1000) % 1.0<0.5)

"""
@brief Returns signals for an LED to perform a sawtooth wave
@details Given an input time, returns a signal that can 
    be interpreted by an LED as a sawtooth wave using a 
    mathematical modulus and the current time of a process.
@param currentTime The current time a process has taken 
    since the timer was last reset.
@return A number based on a modulus calculation.
"""
def update_STW(currentTime):
    return 100*(currentTime/1000 % 1.0 )

"""
@brief Returns signals for an LED to perform a sine wave
@details Given an input time, returns a signal that can 
    be interpreted by an LED as a sine wave using a 
    mathematical calculation and the current time of a process.
@param currentTime The current time a process has taken 
    since the timer was last reset.
@return A number based on a mathematical calculation.
"""
def update_SW(currentTime):
    return 100*(math.sin(math.pi*currentTime/5000)+ .5)

"""
@brief Determine if a button is pressed
@details Sets the global variable buttonPress equal to true if the interface
    has detected that the button is pressed.
"""
def onButtonPressFCN(IRQ_src):
    ## @brief Boolean that contains whether or not the button is pressed
    ## @details Returns true if the variable has in fact been pressed. The
    ##  variable is reset in the main function back to False so that it can 
    ##  always be set to True in the onButtonPressFCN function.
    global buttonPress
    pinA5.high()
    time.sleep(.25)
    pinA5.low()
    buttonPress = True

   
# The main program should go at the bottom and run 
# continuously until the user exits
if __name__ == '__main__':
#    current_Time = [] ##Sets up time array 
#    for i in range(0,101,1):
#        current_Time += [i/100]
    ## The current state for this iteration of the FSM
    state = 0
    ## The number of iterations performed by the FSM
    runs = 0
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    buttonPress = False
    while(True):
        resetTimer()
        updateTimer()
        try:
        # Attempt to run FSM unless Ctrl + c is hit 
            if (state==0):
                # Run the State 0 code
                print("Welcome to Lab0x01.")
                state = 1
                run =0 #Transition to State 1
            
            elif (state==1):
                if (runs == 1):
                    print ("Running State 1: To use this program, press the blue user button (B1) to continue.")
                if (buttonPress == True):
                    state = 2 #Transition to State 2
                    run = 0 
                    buttonPress = False
            
            elif (state==2):
                if (run == 0):
                    print ("Running State 2:Square Wave.")
                while currentTime < 1000:
                    t2ch1.pulse_width_percent(update_SQW(currentTime))
                    updateTimer()
                resetTimer()
                run += 1
                if (buttonPress == True):
                    state = 3
                    run = 0 #Transition to State 3
                    buttonPress = False
            
            elif (state==3):
                if (run == 0):
                    print ("Running State 3:Sine Wave.")
                while currentTime < 10000:
                    if(buttonPress == True):
                        break
                    t2ch1.pulse_width_percent(update_SW(currentTime))
                    updateTimer()
                resetTimer()
                run+=1
                if (buttonPress == True):
                    state = 4 #Transition to State 4
                    run = 0
                    buttonPress = False
                
            elif (state==4):
                if (run == 0):
                    print ("Running State 4:Sawtooth Wave.")
                while currentTime < 1000:
                     t2ch1.pulse_width_percent(update_STW(currentTime))
                     updateTimer()
                resetTimer()
                run += 1
                current_Time = utime.ticks_ms()
                if (buttonPress == True):
                    state = 2
                    run = 0#Transition to State 2
                    buttonPress = False
            
            runs+=1         # increment the time counter
                        
    # Look for a keyboard interrupt to end the program
        except KeyboardInterrupt:
            break
    print ("Program Terminating.")
    print("Done.")