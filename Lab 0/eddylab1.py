"""
Author: Eddy Rodriguez 
Date: 9/21/2021 
"""
import utime
def fib (idx):
    '''
     @brief This function calculates a Fibonacci number at a specific index.
     @param idx An integer specifying the index of the desired
     Fibonacci number
     '''
    if idx == 0: # sets initial condition for fibonacci number 0
        return 0
    if idx == 1:# sets initial condition for fibonacci number 1
        return 1
    f_0 = 0 #hardcode initial fibonacci values 
    f_1 = 1
    i=2
    while (i <=idx): #this while loop will calculate the fibonacci sequence for values 2 and up
        
        if i== 2:#Condition for the fibonacci number 2 has to be inside while loop as it initiates it
            f_n = f_1 + f_0 
            f_minus_2 = f_1
        else:
            f_minus_1 = f_n #fibonacci sequence 
            f_n = f_minus_1 + f_minus_2
            f_minus_2 = f_minus_1
        i = i + 1
    return f_n
startTime  = utime.ticks_ms()
stopTime = utime.ticks_ms()
duration = utime.ticks_diff(stopTime,startTime)
        
        




if __name__ == '__main__':
 # Replace the following statement with the user interface code
 # that will allow testing of your Fibonacci function. Any code
# within the if __name__ == '__main__' block will only run when
 # the script is executed as a standalone program. If the script
 # is imported as a module the code block will not run.

 ## The index of the Fibonacci number selected by the user
    
    idx = -1 #hard code first idx to initialize while loop for interface code
    while (idx <0): # this while loop will keep asking the user for a positve idx value if the condition is not meet then the while loop will continue 
        idx = input('Hey Pick a number or type quit to leave: ') #This will prompt the user for an input
        if idx == 'quit' : #if the user types quit then condition will break while loop
            break
        idx = int(idx) #if a nugative value is typed then error message will be returned
        if idx <0:
            print("Error must be a postive number")
        continue
    print ('Fibonacci number at '
           'index {:} is {:}.'.format(idx,fib(idx)))
    #This prints the fibonacci sequence and corresponding number