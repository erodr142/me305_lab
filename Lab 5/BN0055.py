# -*- coding: utf-8 -*-
'''
    @file       BN0055.py
    @brief      This file is the driver for the Bosh BNO055 accelerometer.
    @details    The BOSH BNO0055 Intelligent 9-axis absolute orientation sensor
                uses a gyroscope, a versatile,accelerometer and a 
                full performance geomagnetic sensor. These 3 sensors are used to 
                capture the Euler angles and angular velocity. This device can also be 
                calibrated. 
                
    
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 8, 2021
'''
"""
Created on Tue Nov  2 12:23:30 2021

@author: Chloe 
@author Eddy 
"""
from pyb import I2C
import utime
class BNO055:
    #Test code 
    def __init__(self,i2c):
        '''
            @brief Constructor that initializes objects for BNO055 Bosh 
        
            @param i2c object created attached to the IMU bus
               
        '''
        self.i2c = I2C(1)
        
        self.heading = 0    #Euler psi 0-360 deg
        self.roll = 0   #Euler phi -180 - 180 deg
        self.pitch = 0  #Euler beta 0- 90 deg
        
        self.vel_x = 0#Angular velocity in x direction deg/sec
        self.vel_y = 0#Angular velocity in y direction deg/sec
        self.vel_z = 0#Angular velocity in z direction deg/sec
        
        
    def mode(self,mode):
        '''
            @brief method that changes the operating mode of the IMU
            @details The BNO055 IMU has a few different modes. Mode '12',NDOF,
                     is a fusion mode with 9 degrees of freedom where the fused 
                     absolute orientation data is calculated from accelerometer, 
                     gyroscope and the magnetometer. 
            @param mode The argument that is passed through to change the BNO055 mode.   

        '''
        
        self.i2c.mem_write(mode,0x28,0x3D)
        
    def calibration_status(self):
        '''
            @brief method to retrieve the calibration status from the IMU
            @details Outputs the Calibration status of the BNO055 IMU in hexadecimal 
                     This IMU requires that all 3 integrated components be calibrated for 
                     the device to return accurate values. The status of the calibration 
                     can be read:3 indicates fully calibrated; 0 indicates not calibrated.
            @return  Returns the calibration status of the device in hexidecimal format. 
            
        '''
        self.cal_status = self.i2c.mem_read(1,0x28,0x35)
        
        return self.cal_status
        
    def calibration_coef(self):
        '''
            @brief   A method to retrieve the calibration coefficients from the IMU as binary data.
            @details These are the calibration coefficients for the accelerometer offset , magnetometer offset, 
                     gyroscope offset, and magnetmeter radius. These will change based on the BNO0055 calibration. 
                     Storing these coefficients is hepfull as they can be called to calibrate a new device. 
            @return  Returns the calibration coefficients for the 22 different variables. 

        '''
        self.calibration_coeff = self.i2c.mem_read(22, 0x28, 0x55)
        return self.calibration_coeff
    
    def calibration_coef_write(self,cal_coeff_stored):
         '''
             @brief  A method to write the calibration coefficients to the IMU from pre-recorded binary data
             @details This function is used to write the current calibration coefficients for the BNO055 Bosh IMU 
                      so they can be used at a later time if the IMU device needs to be recalibrated. 
             
         '''
         # Note to self:           adress number of address it's calling 
         #                             0x28 is the address of the nucleo board 
         #                                   0x55 is the address from where the data is being pulled. Look BNO055 datasheet 
         self.i2c.mem_write(cal_coeff_stored, 0x28, 0x55) #Note might give error 
    def euler(self):
        '''
            @brief A method to read Euler angles from the IMU to use as state measurements
            @details The fuction calls the Euler angles for the heading,0*-360* deg, the roll,-180* - 180* deg, 
                     and pitch, 0*-90*deg. 
                     Heading – Rotation about the Z axis
                     Pitch –Rotation about the X axis
                     Roll – Rotation about the Y axis
            @return  Returns the heading, pitch, and roll in hexadecimal. 
            
        
        '''

        self.eul_bytes = self.i2c.mem_read(6,0x28,0x1A)
        self.heading = self.eul_bytes[0] | self.eul_bytes[1] << 8  # EUL_heading
        self.roll    = self.eul_bytes[2] | self.eul_bytes[3] << 8  # EUL_roll
        self.pitch   = self.eul_bytes[4] | self.eul_bytes[5] << 8  # EUL_pitch
        return (self.heading, self.roll, self.pitch)
    
    def ang_vel(self):
        '''
            @brief   A method to read angular velocity from the IMU to use as state measurements.
            @details The funtion calls the angular velocity in the x,y,and z direction.
            @returns Returns the angular velocity in the x,y, and z direction in hexidecimal. 
        '''
        self.vel_bytes = self.i2c.mem_read(6, 0x28, 0x14) # Gyroscope reads in rad/sec or dps adress is 0x19 - 0x14 
        self.vel_x    = self.vel_bytes[0] | self.vel_bytes[1] << 8  # gyroscope angular velocity x
        self.vel_y    = self.vel_bytes[2] | self.vel_bytes[3] << 8  # gyroscope angular velocity y
        self.vel_z    = self.vel_bytes[4] | self.vel_bytes[5] << 8  # gyroscope angular velocity z
        
        
        return (self.vel_x, self.vel_y,self.vel_z)
    def convert_hex_to_int(self,item):
        '''
            @brief   Converts hexidecimal to binary 
            @details Converting from hexidecimal to binary so values are easier to read 
            @return  Returns the binary value 
        '''
        if item >32767:
            item -= 65536
        item /= 16
        return item 
if __name__ == '__main__':
    
    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)
    imu.mode(12) #NDOF mode 9-deg of freedom 
    Stored_cal_coeff = False
    while True:
        cal_status = imu.calibration_status()
        while(cal_status[0] != 0b11111111 and Stored_cal_coeff == False):#Checks if device 
            print('Calibration process is in progress',bin(cal_status[0]))
            cal_status = imu.calibration_status()
            if (cal_status[0] == 0b11111111): # Checks if device is calibrated
                print('Calibration process is in progress', bin(cal_status[0]))
                print('Device is calibrated')
                utime.sleep(2)
                cal_coeff = imu.calibration_coef()
                imu.calibration_coef_write(cal_coeff)
                Stored_cal_coeff = True
        if(cal_status[0] != 0b11111111 and Stored_cal_coeff == True):
            imu.calibration_coef_write(cal_coeff)
        heading,pitch,roll = imu.euler()
        heading =   imu.convert_hex_to_int(heading) #values are in degress
        pitch = imu.convert_hex_to_int(pitch)
        roll =  imu.convert_hex_to_int(roll) 
        print('Heading',heading, 'pitch', pitch, 'roll',roll)
        utime.sleep(2)
            
            
        vel_x,vel_y,vel_z = imu.ang_vel()
        vel_x = imu.convert_hex_to_int(vel_x) #I think it's printing in degree/sec 
        vel_y = imu.convert_hex_to_int(vel_y)
        vel_z = imu.convert_hex_to_int(vel_z)
        #print('velocity x ',vel_x,'velocity y ',vel_y,'velocity z ',vel_z)
            